use('Estudiante');

db.estudiantes.insertOne({
  _id: ObjectId("65109795afcb8d5f10fdc849"),
  nombre: "Ana García",
  correo: "ana.garcia@example.com",
  edad: 21,
  genero: "Femenino",
  direccion: "456 Avenida del Sol",
  ciudad: "Ciudad Alegre",
  telefono: "123-456-7890",
  materias: ["Matemáticas", "Historia", "Ciencias"],
  notas: {
    matematicas: 95, historia: 88, ciencias: 92
  }
})

db.estudiantes.insertMany([{
  nombre: "María López",
  correo: "maria.lopez@example.com",
  edad: 20,
  genero: "Femenino",
  direccion: "321 Calle de la Luna",
  ciudad: "Ciudad Serena",
  telefono: "987-654-3210",
  materias: ["Geografía", "Literatura", "Arte"],
  notas: {
    geografia: 92, literatura: 85, arte: 78
  }
}, {
  nombre: "Carlos Rodríguez",
  correo: "carlos.rodriguez@example.com",
  edad: 23,
  genero: "Masculino",
  direccion: "234 Avenida del Mar",
  ciudad: "Ciudad Costera",
  telefono: "777-777-7777",
  materias: ["Historia", "Música", "Educación Física"],
  notas: {
    historia: 88, musica: 75, educacion_fisica: 92
  }
}, {
  nombre: "Laura Martínez",
  correo: "laura.martinez@example.com",
  edad: 19,
  genero: "Femenino",
  direccion: "567 Calle del Río",
  ciudad: "Ciudad Fluvial",
  telefono: "444-444-4444",
  materias: ["Química", "Biología", "Matemáticas"],
  notas: {
    quimica: 89, biologia: 94, matematicas: 91
  }
}, {
  nombre: "Luis Sánchez",
  correo: "luis.sanchez@example.com",
  edad: 24,
  genero: "Masculino",
  direccion: "876 Avenida de la Montaña",
  ciudad: "Ciudad de la Montaña",
  telefono: "888-888-8888",
  materias: ["Física", "Química", "Informática"],
  notas: {
    fisica: 85, quimica: 90, informatica: 88
  }
}, {
  nombre: "Sofía Torres",
  correo: "sofia.torres@example.com",
  edad: 20,
  genero: "Femenino",
  direccion: "432 Calle de las Flores",
  ciudad: "Ciudad Jardín",
  telefono: "999-999-9999",
  materias: ["Arte", "Historia del Arte", "Danza"],
  notas: {
    arte: 92, historia_del_arte: 87, danza: 90
  }
}, {
  _id: ObjectId("6510c0a1d417b5de7e183e5c"),
  nombre: "David Gómez",
  correo: "david.gomez@example.com",
  edad: 22,
  genero: "Masculino",
  direccion: "765 Avenida de los Sueños",
  ciudad: "Ciudad de los Sueños",
  telefono: "555-555-5555",
  materias: ["Astronomía", "Física", "Matemáticas"],
  notas: {
    fisica: 88, matematicas: 92, astronomia: 85
  }
}, {
  nombre: "Elena Ramírez",
  correo: "elena.ramirez@example.com",
  edad: 21,
  genero: "Femenino",
  direccion: "987 Calle de los Sueños",
  ciudad: "Ciudad de los Sueños",
  telefono: "111-111-1111",
  materias: ["Literatura", "Filosofía", "Psicología"],
  notas: {
    literatura: 90, filosofia: 86, psicologia: 88
  }
}, {
  nombre: "Javier Vargas",
  correo: "javier.vargas@example.com",
  edad: 23,
  genero: "Masculino",
  direccion: "543 Avenida de los Deportes",
  ciudad: "Ciudad Deportiva",
  telefono: "222-222-2222",
  materias: ["Educación Física", "Nutrición", "Salud"],
  notas: {
    educacion_fisica: 87, nutricion: 90, salud: 88
  }
}, {
  nombre: "Isabel Hernández",
  correo: "isabel.hernandez@example.com",
  edad: 20,
  genero: "Femenino",
  direccion: "678 Calle del Conocimiento",
  ciudad: "Ciudad Sabia",
  telefono: "333-333-3333",
  materias: ["Biología", "Química", "Medicina"],
  notas: {
    biologia: 92, quimica: 89, medicina: 87
  }
}])

// INDEX

db.estudiantes.createIndex({nombre: 1})

db.estudiantes.createIndex({correo: 1}, {unique: true})

db.estudiantes.createIndex({ubicacion: "2dsphere"})

db.estudiantes.createIndex({nombre: "text", direccion: "text"})

db.estudiantes.createIndex({edad: 1},
    {partialFilterExpression: {edad: {$gte: 18}}})

db.estudiantes.createIndex({nombre: 1, edad: -1})

db.estudiantes.getIndexes()

// SELEC

db.estudiantes.find({})

db.estudiantes.find({}, {nombre: 1, correo: 1, _id: 0})

db.estudiantes.find({"notas.matematicas": {$gt: 90}})

db.estudiantes.find({
  edad: {$gt: 21}, materias: {$in: ["Literatura", "Historia"]}
}, {nombre: 1, correo: 1, _id: 0})

db.estudiantes.find().sort({edad: 1}).limit(5)

db.estudiantes.find({
  $or: [{edad: {$lt: 20}}, {edad: {$gt: 23}}]
})

db.estudiantes.find({direccion: /Avenida/})

db.estudiantes.find({correo: /example\.com/})

db.estudiantes.find({
  materias: {
    $all: ["Matemáticas", "Historia", "Ciencias"]
  }
})

db.estudiantes.find({"notas.quimica": {$exists: true}})

db.estudiantes.find({}, {_id: 0, nombre: 1, notas: 1}).sort(
    {"notas.matematicas": -1}).limit(1)

db.estudiantes.find({"notas.fisica": {$exists: true}},
    {_id: 0, nombre: 1, edad: 1}).sort({edad: 1}).limit(1)

// AGREGAT

db.estudiantes.aggregate([{
  $group: {
    _id: "$genero", edadMinima: {$min: "$edad"}, edadMaxima: {$max: "$edad"}
  }
}])

db.estudiantes.aggregate([{
  $group: {
    _id: "$ciudad", totalEstudiantes: {$sum: 1}
  }
}, {
  $sort: {totalEstudiantes: -1}
}])

db.estudiantes.aggregate([{
  $group: {
    _id: "$genero", edadPromedio: {$avg: "$edad"}
  }
}])

db.estudiantes.aggregate([{
  $lookup: {
    from: "estudiantes",
    localField: "materias",
    foreignField: "materias",
    as: "estudiantes_con_materias_en_comun"
  }
}, {
  $match: {
    estudiantes_con_materias_en_comun: {$ne: []}
  }
}])

db.estudiantes.aggregate([{
  $group: {
    _id: "$edad", cantidad: {$sum: 1}
  }
}, {
  $sort: {_id: 1}
}])

db.estudiantes.aggregate([{
  $unwind: "$notas"
}, {
  $group: {
    _id: "$notas", promedio: {$avg: "$notas"}
  }
}])

db.estudiantes.aggregate([{
  $project: {
    nombre: 1, promedioNotas: {
      $avg: ["$notas.matematicas", "$notas.historia", "$notas.ciencias"]
    }
  }
}, {
  $sort: {promedioNotas: -1}
}, {
  $limit: 5
}])

db.estudiantes.aggregate([{
  $group: {
    _id: {
      genero: "$genero", ciudad: "$ciudad"
    }, totalEstudiantes: {$sum: 1}
  }
}])

db.estudiantes.aggregate([{
  $unwind: "$materias"
}, {
  $group: {
    _id: "$materias", cantidad: {$sum: 1}
  }
}, {
  $sort: {cantidad: -1}
}, {
  $limit: 3
}])

db.estudiantes.aggregate([{
  $match: {
    notas: {
      $exists: true
    }
  }
}, {
  $group: {
    _id: "$nombre", materias: {$push: "$materias"}
  }
}, {
  $match: {
    $expr: {
      $eq: [{$size: "$materias"}, {$size: {$setUnion: "$materias"}}]
    }
  }
}])

// UPDATE

db.estudiantes.updateMany({}, {$inc: {edad: 1}})

db.estudiantes.updateMany({ciudad: "Ciudad Alegre"},
    {$set: {direccion: "123 Calle Nueva"}})

db.estudiantes.updateMany({}, {$addToSet: {materias: "Inglés"}})

db.estudiantes.updateMany({edad: {$lt: 25}},
    {$pull: {materias: "Educación Física"}})

db.estudiantes.updateMany({genero: "Masculino"},
    {$inc: {"notas.matematicas": 5, "notas.historia": 5, "notas.ciencias": 5}})

db.estudiantes.updateMany({ciudad: "Ciudad Serena"},
    {$inc: {"notas.geografia": 10, "notas.literatura": 10, "notas.arte": 10}})

db.estudiantes.updateMany({edad: {$gt: 23}}, {$set: {genero: "No binario"}})

db.estudiantes.updateMany({notas: {$not: {$elemMatch: {$gte: 70}}}},
    {$unset: {materias: ""}})

db.estudiantes.updateMany({notas: {$not: {$elemMatch: {$lt: 70}}}},
    {$set: {estatus: "Aprobado"}})

// DELETE
db.estudiantes.find({})

db.estudiantes.deleteOne({nombre: "Ana García"})

db.estudiantes.deleteOne({}, {sort: {_id: -1}})

db.estudiantes.deleteOne({"notas.quimica": 89, ciudad: "Ciudad Fluvial"})

db.estudiantes.deleteMany(
    {direccion: /Avenida/, "notas.informatica": {$lt: 89}})

db.estudiantes.deleteMany({
  $expr: {
    $and: [{
      $lt: [{
        $avg: ["$notas.matematicas", "$notas.historia", "$notas.ciencias"]
      }, 70]
    }, {
      $lt: [{$avg: ["$notas.geografia", "$notas.literatura", "$notas.arte"]},
        70]
    }]
  }
})

db.estudiantes.deleteMany({
  $and: [{
    $expr: {
      $lt: [{
        $avg: ["$notas.matematicas", "$notas.historia", "$notas.ciencias"]
      }, 95]
    }
  }, {
    $expr: {
      $lt: [{
        $avg: ["$notas.geografia", "$notas.literatura", "$notas.arte"]
      }, 95]
    }
  }]
})
