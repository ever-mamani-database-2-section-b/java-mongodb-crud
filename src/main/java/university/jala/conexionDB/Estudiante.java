package university.jala.conexionDB;

import java.util.List;
import java.util.Map;

/**
 * La clase Estudiante representa a un estudiante con información personal y académica.
 *
 * @author Ever Mamani Vicente.
 */
public class Estudiante {

  private String id;
  private String nombre;
  private String correo;
  private int edad;
  private String genero;
  private String direccion;
  private String ciudad;
  private String telefono;
  private List<String> materias;
  private Map<String, Integer> notas;

  public Estudiante(String id, String nombre, String correo, int edad, String genero,
      String direccion, String ciudad, String telefono, List<String> materias,
      Map<String, Integer> notas) {
    this.id = id;
    this.nombre = nombre;
    this.correo = correo;
    this.edad = edad;
    this.genero = genero;
    this.direccion = direccion;
    this.ciudad = ciudad;
    this.telefono = telefono;
    this.materias = materias;
    this.notas = notas;
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getNombre() {
    return nombre;
  }

  public void setNombre(String nombre) {
    this.nombre = nombre;
  }

  public String getCorreo() {
    return correo;
  }

  public void setCorreo(String correo) {
    this.correo = correo;
  }

  public int getEdad() {
    return edad;
  }

  public void setEdad(int edad) {
    this.edad = edad;
  }

  public String getGenero() {
    return genero;
  }

  public void setGenero(String genero) {
    this.genero = genero;
  }

  public String getDireccion() {
    return direccion;
  }

  public void setDireccion(String direccion) {
    this.direccion = direccion;
  }

  public String getCiudad() {
    return ciudad;
  }

  public void setCiudad(String ciudad) {
    this.ciudad = ciudad;
  }

  public String getTelefono() {
    return telefono;
  }

  public void setTelefono(String telefono) {
    this.telefono = telefono;
  }

  public List<String> getMaterias() {
    return materias;
  }

  public void setMaterias(List<String> materias) {
    this.materias = materias;
  }

  public Map<String, Integer> getNotas() {
    return notas;
  }

  public void setNotas(Map<String, Integer> notas) {
    this.notas = notas;
  }

  @Override
  public String toString() {
    return "Estudiante{" +
        "id='" + id + '\'' +
        ", nombre='" + nombre + '\'' +
        ", correo='" + correo + '\'' +
        ", edad=" + edad +
        ", genero='" + genero + '\'' +
        ", direccion='" + direccion + '\'' +
        ", ciudad='" + ciudad + '\'' +
        ", telefono='" + telefono + '\'' +
        ", materias=" + materias +
        ", notas=" + notas +
        '}';
  }
}