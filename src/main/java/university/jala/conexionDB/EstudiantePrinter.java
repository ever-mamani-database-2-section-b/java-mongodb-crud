package university.jala.conexionDB;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import java.util.List;

public class EstudiantePrinter {

  /**
   * Método para imprimir una lista de estudiantes en formato JSON con formato legible.
   *
   * @param estudiantes La lista de estudiantes que se desea imprimir.
   */
  public static void printEstudiantesInJSONFormat(List<Estudiante> estudiantes) {
    System.out.println("\n");
    ObjectMapper objectMapper = new ObjectMapper();
    objectMapper.enable(SerializationFeature.INDENT_OUTPUT);

    try {
      String estudiantesJson = objectMapper.writeValueAsString(estudiantes);
      System.out.println(estudiantesJson);
    } catch (Exception e) {
      System.err.println("Error al convertir la lista de estudiantes a JSON: " + e.getMessage());
    }
    System.out.println("\n");
  }
}
