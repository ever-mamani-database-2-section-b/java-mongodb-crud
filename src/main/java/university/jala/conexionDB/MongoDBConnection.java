package university.jala.conexionDB;

import com.mongodb.ConnectionString;
import com.mongodb.MongoClientSettings;
import com.mongodb.MongoException;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoDatabase;

/**
 * La clase MongoDBConnection se encarga de establecer la conexión con la base de datos MongoDB y
 * proporciona una instancia de la base de datos.
 *
 * @author Ever Mamani Vicente.
 */
public class MongoDBConnection {

  private static final String MONGODB_URI = "mongodb+srv://root:root@jalaprueva.q1elkam.mongodb.net/?retryWrites=true&w=majority";
  private static final String DATABASE_NAME = "Estudiante";
  private static MongoDBConnection instance;
  private final MongoDatabase database;

  /**
   * Constructor privado para garantizar que solo se pueda crear una instancia de la conexión.
   *
   * @throws MongoDBException Si ocurre un error al conectar con MongoDB.
   */
  private MongoDBConnection() throws MongoDBException {
    try {
      MongoClientSettings settings = MongoClientSettings.builder()
          .applyConnectionString(new ConnectionString(MONGODB_URI))
          .build();
      MongoClient mongoClient = MongoClients.create(settings);
      database = mongoClient.getDatabase(DATABASE_NAME);
    } catch (MongoException e) {
      throw new MongoDBException("Error al conectar a MongoDB");
    }
  }

  /**
   * Método para obtener la instancia única de MongoDBConnection.
   *
   * @return La instancia de MongoDBConnection.
   * @throws MongoDBException Si ocurre un error al conectar con MongoDB.
   */
  public static MongoDBConnection getInstance() throws MongoDBException {
    if (instance == null) {
      synchronized (MongoDBConnection.class) {
        if (instance == null) {
          instance = new MongoDBConnection();
        }
      }
    }
    return instance;
  }

  /**
   * Método para obtener la base de datos MongoDB.
   *
   * @return La instancia de la base de datos.
   */
  public MongoDatabase getDatabase() {
    return database;
  }
}
